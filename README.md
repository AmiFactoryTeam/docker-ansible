## Very simple Docker container with Ansible installed
This image is useful if you don't want to install Ansible locally but need to run Ansible to make some provisioning.

### Base
This container is based on [ubuntu:16.04 image](https://hub.docker.com/_/ubuntu) and [Ansible 2.0~](https://launchpad.net/ubuntu/xenial/+source/ansible)

## Usage
### Run Ansible in Docker container in interactive mode
```
docker run --rm -v <private key file>:/root/.ssh/id_rsa -v <project dir>:/infra --env-file <docker env file> -it  registry.gitlab.com/amifactoryteam/docker-ansible:latest
```
were:
* 'private key file' - absolute path to a ssh private key. This key will be used to get ssh-access for Ansible to connect to target host;
* 'project dir' - absolute path to directory, that contains Ansible playbook(s);
* 'docker env file' - absolute path to .docker-env file. This file usually contains environment and secret variables, that Ansible will use during target host provisioning. This is optional param.

### Preparation
After you run Ansible Image, you need to prepare the ssh connection. Run following:
1. Run ssh deamon:
```
eval $(ssh-agent -s)
```
2. Add your private key to ssh deamon:
```
ssh-add
```

### Run Ansible
Now everythink ready to run Ansible. Move to your project dir:
```
cd /infra
```
and run Ansible (We suppose project dir contains Ansible playbook):
```
ansible-playbook playbook.yml
```

### Happy provisioning!